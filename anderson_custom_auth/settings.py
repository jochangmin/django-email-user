"""
Django settings for techtree project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import json
import os
from django.conf.global_settings import FILE_UPLOAD_HANDLERS

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DEBUG = True
TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['localhost']

# Customize Authentication
AUTHENTICATION_BACKENDS = ['email_auth.backend.EmailUserBackend']
AUTH_USER_MODEL = 'email_auth.EmailUserModel'


DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.mysql",
        "NAME": "email_auth",
        "USER": "test",
        "PASSWORD": "1234",
        "HOST": "localhost",
        "PORT": "5432",
        'OPTIONS': {
            "init_command": "SET foreign_key_checks = 0;",
        },
    }
}


# Application definition
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'email_auth',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'anderson_custom_auth.urls'
WSGI_APPLICATION = 'anderson_custom_auth.wsgi.application'


# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

ADMIN_MEDIA_PREFIX = '/static/admin/'

NOSE_ARGS = ['--nocapture',
             '--nologcapture', ]

SECRET_KEY = '+o)%kimoi=k!971gh58r#37&g61fjt+il+id#0tgh9s2_@w&+!'
