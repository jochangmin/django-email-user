from django.contrib.auth import get_user_model, authenticate, login
from django.contrib.auth.forms import UserChangeForm
from django.test import Client
from django.test.testcases import TestCase

from email_auth.forms import EmailUserCreationForm, EmailUserChangeForm
from email_auth.models import EmailUserModel


class AndersonEmailAuthenticatorTest(TestCase):
    def setUp(self):
        model = get_user_model()
        model.objects.all().delete()

    def test_get_user_model(self):
        model = get_user_model()
        self.assertEqual(model, EmailUserModel)

    def test_create_superuser(self):
        model = get_user_model()
        user = model.objects.create_superuser('test@local.com',
                                              '1234')
        user2 = model.objects.get(pk=user.pk)
        self.assertEqual(user.pk, user2.pk)
        self.assertEqual('test@local.com', user2.email)
        self.assertTrue(user2.is_staff)
        self.assertTrue(user2.is_superuser)

    def test_authenticate(self):
        model = get_user_model()
        user = model.objects.create_user('test@localhost.com', '1234',
                                         first_name='First', last_name='Last')
        self.assertEqual('First Last', user.get_full_name())
        self.assertEqual('test@localhost.com', user.get_short_name())
        self.assertEqual('test@localhost.com', user.email)
        self.assertIsNotNone(user.date_joined)

        # It should be authenticated
        user2 = authenticate(email='test@localhost.com', password='1234')
        self.assertEqual(user.pk, user2.pk)
        self.assertEqual(user.email, user2.email)
        self.assertEqual(user.email, user2.get_username())

        # It should be unauthenticated
        user3 = authenticate(email='xxxnothing@nothingxx.com', password='3332')
        self.assertEqual(None, user3)

    def test_login(self):
        model = get_user_model()
        user = model.objects.create_user('test@localhost.com', '1234',
                                         first_name='First', last_name='Last')

        client = Client()
        self.assertTrue(client.login(email='test@localhost.com', password='1234'))

    def test_user_form(self):
        data = {'email': 'test@localhost.com', 'password1': '1234', 'password2': '1234'}

        # User Creation
        form = EmailUserCreationForm(data)
        self.assertEqual(True, form.is_valid())
        user = form.save()
        self.assertIsNotNone(user)
        self.assertEqual('test@localhost.com', user.get_username())
        self.assertEqual('test@localhost.com', user.email)
        self.assertEqual('', user.first_name)
        self.assertEqual('', user.last_name)

        # Creates a duplicate user
        form = EmailUserCreationForm(data)
        self.assertFalse(form.is_valid())

        # Changes a user
        form = EmailUserChangeForm({'first_name': 'First',
                                    'last_name': 'Last',
                                    'email': 'test@localhost.com',
                                    'date_joined': user.date_joined}, instance=user)
        self.assertEqual(True, form.is_valid())
        user = form.save()
        self.assertEqual('test@localhost.com', user.email)
        self.assertEqual('First', user.first_name)
        self.assertEqual('Last', user.last_name)
