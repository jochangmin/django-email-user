# Django Email User Model #

* Support the latest Django version
* Provide Custom Email User (with username included)
* 'username' field has been removed.
* 'email' field is used as 'username' field

# Installation #

    pip install django-email-authenticator

# How to Setup #

### Settings.py ###

Add 'email_auth' to INSTALLED_APPS in settings.py.

```
#!python

AUTHENTICATION_BACKENDS = ['email_auth.backend.EmailUserBackend']
AUTH_USER_MODEL = 'email_auth.EmailUserModel'

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'email_auth',
)
```


### EmailUserCreationForm ###

```
#!python

from email_auth.forms import EmailUserCreationForm
data = {}
data['email'] = 'tester4353@gmail.com'
data['password1'] = '1234'
data['password2'] = '1234'

form = EmailUserCreationForm(data)
form.is_valid() # True
form.save()
```

### Extending EmailUserModel ###

You can extend EmailUserModel for adding more fields. 


```
#!python
from custom_auth.models import EmailAbstractUser

class MyUserModel(EmailAbstractUser):
    class Meta:
        db_table = 'custom_user'

```
# Tools #

### user_exists function ###

You can check if the email exists. 

    user_exists('test@google.com') # True or False

# Errors #

### MYSQL 150 Error ###

When you run 'syncdb' Mysql could raise 150 Error.

    _mysql_exceptions.OperationalError: (
        1005, "Can't create table '\\db_name\\.#sql-4a8_ab' (errno: 150)"
    )

The error is related to the database engine. 

See the following link. 

https://docs.djangoproject.com/en/1.8/ref/databases/#storage-engines

You need to set database engine. 



```
#!python

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.mysql",
        "NAME": "db_name",
        "USER": "username",
        "PASSWORD": "password",
        "HOST": "localhost",
        "PORT": 3306,
        "OPTIONS": {
            "init_command": "SET storage_engine=MyISAM"
        }
    }
}
```


### Cannot add foreign key constraint ERROR

If you see the following error.. 

```
django.db.utils.IntegrityError: (1215, 'Cannot add foreign key constraint')
```

The solution is.. 

```
DATABASES = {
'default': {
    ...         
    'OPTIONS': {
         "init_command": "SET foreign_key_checks = 0;",
    },
 }
}
```