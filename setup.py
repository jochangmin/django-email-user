from distutils.core import setup

setup(
    name='django-email-authenticator',
    version='0.4.9',
    packages=['email_auth'],
    url='https://bitbucket.org/jochangmin/django-email-user',
    license='MIT',
    author='Anderson Jo',
    author_email='a141890@gmail.com',
    description=''
)
